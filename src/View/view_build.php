<?php

if(!isset($module) or !is_string($module)){
    header("HTTP/1.0 500 Internal Server Error");
    echo '<h1>Variável $include não encontrada!</h1>';
    exit;
}

$twig->addFilter(new Twig_Filter('slugify', function( $text ) {
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);
  if (empty($text)) {
    return 'n-a';
  }
  return $text;
}));

$module_data = (empty($module_data)) ? 'none' : $module_data;
$lg = $_SESSION['lg'];
//expressoes para header menu e footer
// $menu = $config->getMenu($lg);
$menu = $config->getMegaMenu($lg);
// foreach ($menu as $key => $value) {
//   if (!empty($menu[$key]['mega'])) {
//     $areas = array_keys($menu[$key]['mega']);
//   }
// }
// $cat = $adm_conection->getCategoryes();
// $exp = $expressions->getExp(array(0,1,2,3,4,22,23,24,25,26,27,28,49,50,51), $_SESSION['lg']);
// $exp = array('nothing' => null);
$exp = $config->getExp(null, $lg);

//adicionando dados de sessao ao array
$twig_data = array_merge(array('menu' => $menu, 'lg' => $lg, 'module' => $module, 'action' => $action, 'parametro' => $param, 'module_data' => $module_data, 'url_base' => URL_BASE, 'is_mobile' => $device->isMobile()), $_SESSION, $exp);

echo $twig->render('Includes/head.html', $twig_data);
//include 'Includes/head.php';
echo $twig->render('Includes/menu.html', $twig_data);
//include 'Includes/menu.hmtl';

if(file_exists('src'.DS.'View'.DS.$module.DS.$action.'.html')){ echo $twig->render($module.DS.$action.'.html', $twig_data); }
//if(file_exists('src'.DS.'View'.DS.$module.DS.$action.'.php')){ include $module.DS.$action . '.php'; }
//else if($adm_conection->getPostBySlug($module)) { include 'Generic'.DS.$action . '.php'; }
else if($module == 'Catalogo') { echo $twig->render($module.DS.'index.html', $twig_data); }
else if($module == 'Site') { echo $twig->render($module.DS.$action.'.html', $twig_data); }
else { echo $twig->render('Includes/404.html', $twig_data); }

echo $twig->render('Includes/footer.html', $twig_data);
//include 'Includes/footer.php';

//setando URL base para o JS
echo "<script>var URL_BASE = '".URL_BASE."';</script>";
echo "<script>var USER_SID = '".$_SESSION['usuario']['id']."';</script>";

//incluindo JS do modulo
$filename = 'js/Controller/'.$module.'.js';

if (file_exists($filename)) {
    echo '<script src="'.URL_BASE.'/'.$filename.'"></script>';
} else {
    //echo $filename;
}

// echo '<pre>';
// print_r($twig_data);
// echo '</pre>';
//
// echo '<pre>';
// print_r($module_data);
// echo '</pre>';

?>
