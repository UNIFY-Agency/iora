<?php

class Resources{

    public $mysql;

    // public static function slug($title){
    //     $slug = strtolower($title);
    //     $slug = strtr($slug,
    //       array (
    //         'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
    //         'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E',
    //         'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ð' => 'D', 'Ñ' => 'N',
    //         'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O',
    //         'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Ŕ' => 'R',
    //         'Þ' => 's', 'ß' => 'B', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
    //         'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
    //         'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
    //         'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
    //         'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y',
    //         'þ' => 'b', 'ÿ' => 'y', 'ŕ' => 'r'
    //       )
    //     );
    //     $slug = preg_replace('/[^a-z0-9]\ -/', '', $slug);
    //     $slug = preg_replace('/[ ]\ -/', '-', $slug);
    //     $slug = str_replace(' ', '_', $slug);
    //     return $slug;
    // }

    public static function slug($text){
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
      $text = preg_replace('~[^-\w]+~', '', $text);
      $text = trim($text, '-');
      $text = preg_replace('~-+~', '-', $text);
      $text = strtolower($text);
      if (empty($text)) {
        return 'n-a';
      }
      return $text;
    }

    public static function ajaxProtect($post, $session){
      //verifico se faz mais de 3 segundos da última chamada para evitar ataques
      if(isset($session['usuario']['lastajaxcall']) && (strtotime('now') - $session['usuario']['lastajaxcall']) < 1){
          echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Chamada com menos de 1 segundos! - Agora: '.date('s', strtotime('now')).', Ultima: '.date('s',$session['usuario']['lastajaxcall'])));
          exit();
      }
      $session['usuario']['lastajaxcall'] = strtotime('now');

      //verifico se o id de uauario que foi passado na chamada e o mesmo que o de sessão
      $session_id = array_pop($post['dados']);
      if($session_id['value'] !== $session['usuario']['id']){
          echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Alerta de tentativa de violação de acesso, tentativa de invazão registrada.'));
          exit();
      }
    }

}
