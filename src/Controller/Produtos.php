<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$prod = new Produtos(new Config());
$loader->get('src/Model/Categorias');
$cat = new Categorias(new Config());
$loader->get('src/Model/Marcas');
$marcas = new Marcas(new Config());
$loader->get('src/Model/Mail');

$mensagem_erro = false;

if (Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  if($action == 'ativar'){
    $prod->ativaProd($_POST['id']);
  } else if($action == 'inativar'){
    $prod->inativaProd($_POST['id']);
  }

  if($action == 'send'){

    $produto = $prod->getProdutoMarca(array_pop($_POST['dados'])['value']);

    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    $html = '<h1>Pedido de orçamento do produto '.$produto['referencia'].'</h1>';
    $html .= '<p>O cliente '.$_POST['dados'][0]['value'].' pediu um orçamento do produto '.$produto['nome'].' no site da Eurotenologia</p>';
    $html .= '<p>Foi enviado o email de contacto '.$_POST['dados'][1]['value'].' com a seguinte mensagem:</p>';
    $html .= '<p>'.$_POST['dados'][2]['value'].'</p>';
    $html .= '<p>Qualquer dúvida entre em contacto.</p>';

    if (SendMail::trySendMail('Pedido de orçamento Eurotenologia', 'mail@unifycloud.pt', $html, new PHPMailer(true))) {
      echo json_encode(array('1', '<strong>Sucesso!</strong> Pedido de orçamento enviado com sucesso.'));
      exit();
    } else {
      echo json_encode(array('0', '<strong>Erro!</strong> O sistema não conseguiu enviar o e-mail.'));
      exit();
    }
    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  }

}

if($action == 'index'){

  $produtos = $prod->readProductsAndAllFamilies();
  $module_data['produtos'] = $produtos;

} else if($action == 'detalhe'){

  $produto = $prod->getProdutoMarca($param);
  $module_data['produto'] = $produto;

  // echo '<pre>';
  // print_r($_POST);
  // echo '</pre>';

} else if($action == 'editar'){

    if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

    $produto = $prod->getProdutoMarca($param);

    $categorias = $cat->readCat();

    // echo '<pre>';
    // print_r($_POST);
    // echo '</pre>';
    // echo '<pre>';
    // print_r($_FILES);
    // echo '</pre>';

    if($_POST){
      if(!empty($_POST['nome']) and !empty($_POST['preco']) and !empty($_POST['categoria']) and !empty($_POST['descricao'])){

          $foto_name = empty($_FILES['foto']['name']) ? '' : date('dmyHis').sha1($_FILES['foto']['name']).'.'.strtolower(pathinfo($_FILES['foto']['name'],PATHINFO_EXTENSION));
          $marca_sku = strtolower(explode(' ', $produto['marca'])[0]);

          $dados['id'] = $param;
          // $dados['referencia'] = $_POST['referencia'];
          $dados['nome'] = $_POST['nome'];
          $dados['preco'] = $_POST['preco'];
          $dados['categoria'] = $_POST['categoria'];
          // $dados['marca'] = $_POST['marca'];
          $dados['descricao'] = $_POST['descricao'];
          $dados['image_url'] = empty($_FILES['foto']['name']) ? '' : URL_BASE."/assets/upload/$foto_name";
          $dados['foto'] = $_SERVER['DOCUMENT_ROOT']."/UNIFY/repositories/eurotecnologia/bo/assets/upload/$foto_name";

          $uploadOk = 1;
          $target_file = $dados['foto'];
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

          // echo '<pre>';
          // print_r($dados);
          // echo '</pre>';
          // die();

          if(!empty($_FILES['foto']['name'])){

            unlink($_SERVER['DOCUMENT_ROOT']."/UNIFY/repositories/eurotecnologia/bo/assets/upload/".array_reverse(explode('/', $produto['image_url']))[0]);

            //$check = getimagesize($_FILES["foto"]["tmp_name"]);
            // if(!$check) {
                // $mensagem_erro = "O arquivo enviado não é uma imagem.";
                // $uploadOk = 0;
            //} else
            if (file_exists($target_file)) {
                $mensagem_erro = "Desculpe esse arquivo já existe.";
                $uploadOk = 0;
            } else if ($_FILES["foto"]["size"] > 5242880) { // 1048576 = 1MB // 5242880 = 5MB
                $mensagem_erro = "Desculpe, esse arquivo é muito grande.";
                $uploadOk = 0;
            } else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "pdf" ) {
                $mensagem_erro = "O sistema aceita somente JPG, JPEG, PNG, GIF e PDF.";
                $uploadOk = 0;
            } else if ($uploadOk == 0) {
                $mensagem_erro = "Desculpe, seu arquivo não foi enviado.";
            } else if ($_FILES['foto']['error'] != 0) {
                $mensagem_erro = "Desculpe, há algo de errado com seu arquivo.";
            } else {
              if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
                $result = $prod->editProd($dados);
                if(substr($result, 0, 4) == 'erro'){
                  switch ($result) {
                      case 'erro':
                        $mensagem_erro = '<strong>Erro!</strong> Houve algum erro interno!';
                        break;
                  }
                } else {
                  header('Location: '.URL_BASE.'/produtos/listar');
                }
              } else {
                  $mensagem_erro = "Desculpe, houve algum erro e seu arquivo não foi enviado.";
              }
            }
          } else {
            $result = $prod->editProd($dados);
            if(substr($result, 0, 4) == 'erro'){
              switch ($result) {
                  case 'erro':
                    $mensagem_erro = '<strong>Erro!</strong> Houve algum erro interno!';
                    break;
              }
            } else {
              header('Location: '.URL_BASE.'/produtos/listar');
            }
          }
      } else {
        $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
      }
    }
  }

  // echo '<pre>';
  // print_r($config->getMegaMenu($_SESSION['lg']));
  // echo '</pre>';

?>
