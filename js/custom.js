jQuery(document).ready(function() {
  //idioma
  jQuery('.lang').click(function() {
    // console.log(jQuery(this).attr('lang'));
    var url = URL_BASE + '/language/change/' + jQuery(this).attr('lang');
    jQuery.post(url, function() {
      location.reload();
    });
  });

  jQuery('.cookie-ok').click(function() {
    //console.log(jQuery(this).attr('class'));
    var url = URL_BASE + '/cookies/allow/';
    jQuery.post(url, function() {
      jQuery('.notice-bottom-bar').hide();
    });
  });

  jQuery('.header-search-mobile').click(function() {
    console.log('clique');
    jQuery('.search-line').toggle("slow");
  });

  $('.dropdown-mega-sub-title').click(function() {

    // console.log($(this).attr('class'));

    if (!$(this).hasClass('active-menu')) {
      $(".dropdown-mega-sub-title").removeClass("active-menu").find('.arrowdown-active').removeClass().addClass('arrowdown').attr('src', URL_BASE + '/assets/img/icons/arrowdown.svg');
      $(this).addClass('active-menu').find('.arrowdown').removeClass().addClass('arrowdown-active').attr('src', URL_BASE + '/assets/img/icons/arrowdown-active.svg');
      // console.log('n tem');
    }

  });

  $('.openmenu100').click(function() {
    $('.menu100').show("slide", {
      direction: 'right'
    }, 500);
    $('.openmenu100').hide();
    $('#mainmenu').hide('fade');
  });
  $('.closemenu100').click(function() {
    $('.menu100').hide("slide", {
      direction: 'right'
    }, 500);
    $('.openmenu100').show();
    $('#mainmenu').show('fade');
  });

  $('.saltar-video').click(function() {
    $('.saltar-video').fadeOut();
    $('.titulo-prod').fadeIn(2500);
    setTimeout(function() {
      $("#prod-menu").trigger("click");
    }, 2000);
  });

  $(".horizontal-scroll-wrapper").on("scroll", function(e) {
    var footer_limit = Math.round($('.footer-delimiter').offset().left);
    if (footer_limit <= $(window).width()) {
      $('#footer').show("slide", {
        direction: 'right'
      }, 500);
      console.log('limite');
    } else {
      $('#footer').hide("slide", {
        direction: 'right'
      }, 500);
    }
    // console.log(footer_limit);
  });


  $('.product-div-mobile').click(function(){
      if ($(this).find('.active-section').css('display') == 'none') {
          $(this).find('.active-section').show();
      }
      else {
          $(this).find('.active-section').hide();
      }
  });

  $('.product-div').click(function(){
      if ($(this).find('.active-section').css('display') == 'none') {
          $(this).find('.active-section').show();
      }
      else {
          $(this).find('.active-section').hide();
      }
  });


  $(document).on('click', '.menu100-2-open', function() {
    $('.menu100-right-1').fadeOut();
    $('.menu100-right-2').fadeIn();
    $('.menu100-p').css('color', '#78614C');
  });

  $(document).on('click', '.menu100-2-close', function() {
    $('.menu100-right-1').fadeIn();
    $('.menu100-right-2').fadeOut();
    $('.menu100-p').css('color', '#a78c72');
  });

  $('.close-cart').click(function(){
      $('.cart').fadeOut();
  });

});
